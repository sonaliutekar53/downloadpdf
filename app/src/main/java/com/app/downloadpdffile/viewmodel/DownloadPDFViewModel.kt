package com.mytestapp.viewmodel

import android.app.Application
import android.app.DownloadManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData


class DownloadPDFViewModel(application: Application) : AndroidViewModel(application) {
    private val context = getApplication<Application>().applicationContext
    var pdfUrl: MutableLiveData<String> = MutableLiveData()


    fun onClicked() {
        pdfUrl.setValue("https://yukon.ca/en/education-and-schools.pdf")
        val url = pdfUrl.value.toString()
        callpdf(url)

    }


    private fun callpdf(url: String) {
        var destination =
                context!!.getExternalFilesDir("")
                        .toString() + "/SonaliPDFFile/"
        val fileName = "TestPDF.pdf"
        destination += fileName

        val uri: Uri = Uri.parse("file://$destination")


        val request = DownloadManager.Request(Uri.parse(url))
        request.setDescription("Downloading....")
        request.setTitle("TestPDF")
        request.setDestinationUri(uri)
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        val manager =
                context.getSystemService(AppCompatActivity.DOWNLOAD_SERVICE) as DownloadManager
        val downloadId = manager.enqueue(request)
        val finalDestination = destination
    }


}